console.log("Rest API!");

// [Section] Javascript Synchronous vs. asynchronous

	//by default JavaScript is synchronous meaning it only executes one statement at a time

console.log("hello world!");

/*conole.log("hello");*/
/*for(let i = 0; i <= 1500; i++){
	console.log(i);
}*/

console.log("I am the console.log after the for loop!");

// async and await - it lets us to proceed or execute more than one statements at the same time

// [Section] Getting all posts
	//The fetch API allows us to asynchronously request for a resour or data
		// fetch() method in javascriot is used to request to the serveer and load information on the webpage
	//Syntax:
		// fetch("apiURL")

	console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

		/*a "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value* /

			// A promise may be in one of 3 possible states: fulfilled, rejected and pending.
			/*
				pending: initial states, neither fulfilled nor rejected response
				fulfilled: operation was completed
				rejected: operation
			*/
		// Syntax
		// fetch("apiURI")
		// .then(response => {what will you do with the response})

		fetch("https://jsonplaceholder.typicode.com/posts")
		//The ".then()" method captures the response object and returns another promise which will be either resolved or rejected
		.then(response => {
			console.log(response);
			return response.json()}
			)
		.then(data => {
			let title = data.map(element => element.title)

			console.log(title);
		})

		//The "async" and 'await' keyword to achieve asynchronous code


		async function fetchData(){
			let result = await(fetch("https://jsonplaceholder.typicode.com/posts"))

			console.log(result);

			let json = await result.json();

			console.log(json);


		}

		fetchData();


        

		//Section - Get a specific post
			//Retrieves a specific post following the REST API(/post/:id)
			// wildcard is where you can put any value, it then creates a link betweend id paramater in the url and the value prodived in the URL
		fetch("https://jsonplaceholder.typicode.com/posts/3", {method: "GET"})
		.then(response => response.json())
		.then(result => console.log(result));

		//[Section] Creating POST
		/*Syntax:
			fetch("apiURL", {necessaryOptions})
			.then(response => response.json())
			.then(result => {codeblock})
		*/

		fetch("https://jsonplaceholder.typicode.com/posts", {
				//REST API method to execute
				method: "POST",
				//headers, it tells us the data type of the our request
				headers: {
					"Content-Type": "application/json"
				},
				//body - contains the data that will be added to the database/the request of the user
				body: JSON.stringify({
					title: "New Post",
					body: "Hello World",
					userId: 1
				})
			})
		.then(response => response.json())
		.then(result => console.log(result));



		//Section - updating a post
		//Put - whole document
		fetch("https://jsonplaceholder.typicode.com/posts/5", {
				method: "PUT",
				headers: {
					'Content-Type' : "application/json"
				},
				body: JSON.stringify({
					title: "updated post",
					body: 'Hello again',
					userId: 1
				})
		})
		.then(response => response.json())
		.then(result => console.log(result));

		//Section - Patch
		//patch - properties

		fetch("https://jsonplaceholder.typicode.com/posts/1", {
			method: "PATCH",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				title: "Update title!"
			})
		})
		.then(response => response.json())
		.then(result => console.log(result));


		// Mini-activity
		 //Using the patch method, change the body property of the document which has the id of 18 to "Hello it's me!"

		//Section - delete a post

		fetch("https://jsonplaceholder.typicode.com/posts/5", {method: "DELETE"})
		.then(response => response.json())
		.then(result => console.log(result));


		//mini activity - using the postman use the put method to replace the doucment that has id of 18.

